SELECT UI.INDEX_NAME AS INDEX_NAME,
INDEX_TYPE,
UI.TABLE_NAME,
COLUMN_NAME||'('||COLUMN_POSITION||')' AS COLUMN_NAME,
BLEVEL,
LEAF_BLOCKS,
DISTINCT_KEYS,
CLUSTERING_FACTOR
FROM USER_INDEXES UI, USER_IND_COLUMNS UIC
WHERE UI.INDEX_NAME=UIC.INDEX_NAME AND UI.TABLE_NAME=UIC.TABLE_NAME
AND (UI.TABLE_NAME='OFFERED_SERVICES'
OR UI.TABLE_NAME='SERVICES'
OR UI.TABLE_NAME='CLEANING_COMPANY'
OR UI.TABLE_NAME='BUILDING'
OR UI.TABLE_NAME='CLEANING_SERVICES')
ORDER BY INDEX_NAME;