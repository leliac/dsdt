------------------------------------------------------------------------------------------------------------------------------------------------------------
| Id  | Operation                               | Name                        | Starts | E-Rows | A-Rows |   A-Time   | Buffers |  OMem |  1Mem | Used-Mem |
------------------------------------------------------------------------------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT                        |                             |      1 |        |     45 |00:00:02.42 |   62457 |       |       |          |
|*  1 |  FILTER                                 |                             |      1 |        |     45 |00:00:02.42 |   62457 |       |       |          |
|   2 |   HASH GROUP BY                         |                             |      1 |     45 |    450 |00:00:02.42 |   62457 |   993K|   993K| 1413K (0)|
|*  3 |    HASH JOIN RIGHT SEMI                 |                             |      1 |    450 |    450 |00:00:00.02 |   62457 |  2546K|  2546K| 1035K (0)|
|   4 |     VIEW                                | VW_NSO_1                    |      1 |      5 |      5 |00:00:00.01 |     343 |       |       |          |
|*  5 |      HASH GROUP BY                      |                             |      1 |      5 |      5 |00:00:00.01 |     343 |  1558K|  1558K| 1156K (0)|
|*  6 |       HASH JOIN                         |                             |      1 |   2000 |   2000 |00:00:00.01 |     343 |  2546K|  2546K| 1651K (0)|
|   7 |        INLIST ITERATOR                  |                             |      1 |        |   1000 |00:00:00.01 |       7 |       |       |          |
|*  8 |         INDEX RANGE SCAN                | CLEANING_COMPANY_REGION_CID |      2 |   1000 |   1000 |00:00:00.01 |       7 |       |       |          |
|*  9 |        HASH JOIN                        |                             |      1 |  20000 |  20000 |00:00:00.01 |     336 |  2546K|  2546K| 1399K (0)|
|* 10 |         TABLE ACCESS FULL               | SERVICES                    |      1 |     10 |     10 |00:00:00.01 |       2 |       |       |          |
|  11 |         TABLE ACCESS FULL               | OFFERED_SERVICES            |      1 |    200K|    200K|00:00:00.01 |     333 |       |       |          |
|* 12 |     HASH JOIN                           |                             |      1 |   9000 |   9000 |00:00:02.40 |   62114 |  2546K|  2546K| 1707K (0)|
|* 13 |      TABLE ACCESS BY INDEX ROWID BATCHED| BUILDING                    |      1 |   4500 |   4500 |00:00:00.01 |      31 |       |       |          |
|* 14 |       INDEX RANGE SCAN                  | BUILDING_CITY               |      1 |   5000 |   5000 |00:00:00.01 |      15 |       |       |          |
|  15 |      TABLE ACCESS BY INDEX ROWID BATCHED| CLEANING_SERVICES           |      1 |     10M|     10M|00:00:02.57 |   62083 |       |       |          |
|* 16 |       INDEX RANGE SCAN                  | CLEANING_SERVICES_DATE      |      1 |     10M|     10M|00:00:00.96 |   26529 |       |       |          |
------------------------------------------------------------------------------------------------------------------------------------------------------------
 
Predicate Information (identified by operation id):
---------------------------------------------------
 
   1 - filter(SUM(""COST"")>=1000)
   3 - access(""SID""=""SID"")
   5 - filter(COUNT(*)>1)
   6 - access(""OS"".""CID""=""CC"".""CID"")
   8 - access((""REGION""='Liguria' OR ""REGION""='Piedmont'))
   9 - access(""OS"".""SID""=""S"".""SID"")
  10 - filter(""CATEGORY""='IndoorCleaning')
  12 - access(""CS"".""BID""=""B"".""BID"")
  13 - filter(""BUILDINGTYPE""<>'Office')
  14 - access(""CITY""='Turin')
  16 - access(""Date"">=TO_DATE(' 2019-01-01 00:00:00', 'syyyy-mm-dd hh24:mi:ss') AND ""Date""<=TO_DATE(' 2019-12-31 00:00:00', 'syyyy-mm-dd 
              hh24:mi:ss'))
