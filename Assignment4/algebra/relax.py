from pygments.lexer import RegexLexer
from pygments.token import *

__all__ = ['RelaXLexer']

class RelaXLexer(RegexLexer):
    name = 'RelaX'
    aliases = ['relax']
    filenames = ['*.relax']

    tokens = {
        'root': [
	    (r'[0-9]+', Number.Integer),
	    (r"'(''|[^'])*'", String.Single),
	    (r'[πσγρ⨝⋉∧∨]|sum|count|date|number|string|group', Name.Builtin),
	    (r'.', Text)
        ]
    }
