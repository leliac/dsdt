INSERT INTO STATE_CHANGE(ChangeId, TimeStamp, PhoneNo, x, y, ChangeType)
	VALUES(4, sysdate, '333000001', 3, 3, 'O');
INSERT INTO STATE_CHANGE(ChangeId, TimeStamp, PhoneNo, x, y, ChangeType)
	VALUES(5, sysdate, '333000004', 5, 5, 'O');
INSERT INTO STATE_CHANGE(ChangeId, TimeStamp, PhoneNo, x, y, ChangeType)
	VALUES(6, sysdate, '333000004', 5, 5, 'C');
INSERT INTO STATE_CHANGE(ChangeId, TimeStamp, PhoneNo, x, y, ChangeType)
	VALUES(7, sysdate, '333000001', 3, 3, 'C');
INSERT INTO STATE_CHANGE(ChangeId, TimeStamp, PhoneNo, x, y, ChangeType)
	VALUES(8, sysdate, '333000010', 3, 3, 'C');
INSERT INTO STATE_CHANGE(ChangeId, TimeStamp, PhoneNo, x, y, ChangeType)
	VALUES(9, sysdate, '333000020', 4, 4, 'O');
INSERT INTO STATE_CHANGE(ChangeId, TimeStamp, PhoneNo, x, y, ChangeType)
	VALUES(10, sysdate, '333000020', 4, 4, 'C');
    
SET SQLFORMAT CSV
SPOOL 'trigger2_telephone.csv'
SELECT * FROM TELEPHONE;
SPOOL OFF
SPOOL 'trigger2_exceptionLog.csv'
SELECT * FROM EXCEPTION_LOG;
SPOOL OFF
