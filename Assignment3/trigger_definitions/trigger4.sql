CREATE OR REPLACE TRIGGER ServiceGuarantee
AFTER UPDATE OF MaxCalls ON CELL
DECLARE
    SumMaxCalls INTEGER;
BEGIN
    SELECT SUM(MaxCalls) INTO SumMaxCalls
    FROM CELL;

    IF(SumMaxCalls <= 30) THEN
        RAISE_APPLICATION_ERROR(-20000, 'Service guarantee violation');
    END IF;
END;
