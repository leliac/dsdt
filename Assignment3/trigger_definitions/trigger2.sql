CREATE OR REPLACE TRIGGER PhoneCall
AFTER INSERT ON STATE_CHANGE
FOR EACH ROW
WHEN(NEW.ChangeType = 'C')
DECLARE
    CellId_ INTEGER;
    x0_ DECIMAL(7,2);
    x1_ DECIMAL(7,2);
    y0_ DECIMAL(7,2);
    y1_ DECIMAL(7,2);
    MaxCalls_ SMALLINT;
    numCalls SMALLINT;
    ExId_ INTEGER;
BEGIN
    SELECT CellId, x0, x1, y0, y1, MaxCalls
    INTO CellId_, x0_, x1_, y0_, y1_, MaxCalls_
    FROM CELL
    WHERE :NEW.x >= x0 AND :NEW.x < x1 AND :NEW.y >= y0 AND :NEW.y < y1;

    SELECT COUNT(*) INTO numCalls 
    FROM TELEPHONE
    WHERE PhoneState = 'Active' AND x >= x0_ AND x < x1_ AND y >= y0_ AND y < y1_;

    IF(numCalls < MaxCalls_) THEN
        UPDATE TELEPHONE
        SET PhoneState = 'Active'
        WHERE PhoneNo = :NEW.PhoneNo;
    ELSE
        SELECT MAX(ExId)+1 INTO ExId_
        FROM EXCEPTION_LOG
        WHERE CellId = CellId_;

        IF(ExId_ IS NULL) THEN
            ExId_ := 1;
        END IF;

        INSERT INTO EXCEPTION_LOG(ExId, CellId, ExceptionType)
        VALUES(ExId_, CellId_, 'C');
    END IF;
END;
