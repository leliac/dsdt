CREATE OR REPLACE TRIGGER Maintenance
BEFORE UPDATE OF MaxCalls ON CELL
FOR EACH ROW
WHEN(NEW.MaxCalls < OLD.MaxCalls)
DECLARE
    numCalls SMALLINT;
BEGIN
    SELECT COUNT(*) INTO numCalls
    FROM TELEPHONE
    WHERE PhoneState = 'Active' AND x >= :OLD.x0 AND x < :OLD.x1 AND y >= :OLD.y0 AND y < :OLD.y1;

    IF(:NEW.MaxCalls < numCalls) THEN
        :NEW.MaxCalls := numCalls;
    END IF;
END;
