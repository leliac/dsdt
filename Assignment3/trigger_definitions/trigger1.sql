CREATE OR REPLACE TRIGGER PhoneOnOff
AFTER INSERT ON STATE_CHANGE
FOR EACH ROW
WHEN(NEW.ChangeType = 'O' OR NEW.ChangeType = 'F')
DECLARE
    DeltaPhone# SMALLINT;
    PhoneExists INTEGER;
BEGIN
    IF(:NEW.ChangeType = 'O') THEN
        INSERT INTO TELEPHONE(PhoneNo, x, y, PhoneState) 
        VALUES(:NEW.PhoneNo, :NEW.x, :NEW.y, 'On');
        DeltaPhone# := 1;
    ELSE
        SELECT COUNT(*) INTO PhoneExists
        FROM TELEPHONE
        WHERE PhoneNo = :NEW.PhoneNo;
                
        IF(PhoneExists = 0) THEN
            RAISE_APPLICATION_ERROR(-20000, 'Cannot delete non-existing phone');
        END IF;
                
        DELETE FROM TELEPHONE
        WHERE PhoneNo = :NEW.PhoneNo;
        DeltaPhone# := -1;
    END IF;

    UPDATE CELL
    SET CurrentPhone# = CurrentPhone#+DeltaPhone#
    WHERE :NEW.x >= x0 AND :NEW.x < x1 AND :NEW.y >= y0 AND :NEW.y < y1;
END;
